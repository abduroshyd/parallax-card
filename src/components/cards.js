import React, {Component} from 'react';
import ParallaxCard from 'react-parallax-card';

class Cards extends Component {
    render() {
        return (
            <div>
                <h1>hello</h1>
                <ParallaxCard
                    label='Label (optional)'
                    enableRotate>
                    <img src='https://s3-us-west-1.amazonaws.com/tachyonsio/img/Blonde-Frank_Ocean.jpeg' width='240' role='presentation' />
                </ParallaxCard>
                {/*style={{ margin: '0 auto', width: 240, height: 240 }}*/}
            </div>
        );
    }
}

export default Cards;